PANDOCOPTS = --standalone --number-sections --toc --toc-depth=2 "-Vdate=$(shell git describe)"
HTMLOPTS = -H sq.css

.SUFFIXES: .md .html .pdf

.md.html:
	pandoc  $(PANDOCOPTS) $(HTMLOPTS) --output $@ $<

.md.pdf:
	pandoc $(PANDOCOPTS) --output $@ $<

all: sq-guide.html sq-guide.pdf

sq-guide.html sq-guide.pdf: sq-guide.md sq.css Makefile
