---
title: "User guide for Sequoia-PGP `sq`"
subtitle: "Keeping private things private"
author: "The Sequoia-PGP project"
documentclass: report
...

# Document status {.unnumbered}

This document is very much a work in progress. Nothing is finished and
final. Parts haven't been
written yet. That said, feedback on what is written, or the structure
of the document, is very much welcome. The source code for this
document is in version control on the `gitlab.com` site at:

```
https://gitlab.com/sequoia-pgp/sq-user-guide/
```

If you find mistakes or missing parts from the outline, please open an
issue or a merge request.

# Prelude: quick start {.unnumbered}

If you're in a hurry to get started, this chapter is for you. This
chapter distils the main content of this guide into examples showing
a small number of common use cases. No explanations.

```{.sh .numberLines}
$ sq key generate --userid="My Name <me@example.com>" --export=key.pgp
$ sq key extract-cert --output=cert.pgp key.pgp
$ ls -l
total 8
-rw-rw-r-- 1 liw liw 1772 Oct 15 16:18 cert.pgp
-rw-rw-r-- 1 liw liw 1967 Oct 15 16:18 key.pgp
-rw-rw-r-- 1 liw liw  476 Oct 15 16:18 key.pgp.rev
```

```{.sh .numberLines}
$ sq sign --signer-key=key.pgp --output=foo.pgp foo.md
$ sq sign --signer-key=key.pgp --detached --output=foo-sig.pgp foo.md
$ ls -l foo*
-rw-r--r-- 1 liw liw 1086 Oct 15 16:19 foo.md
-rw-rw-r-- 1 liw liw 1825 Oct 15 16:20 foo.pgp
-rw-rw-r-- 1 liw liw  325 Oct 15 16:20 foo-sig.pgp
```

```{.sh .numberLines}
$ sq verify --signer-cert=cert.pgp --output=checked.md foo.pgp
Good signature from 84B292ABCE27285B
1 good signature.
$ sq verify --signer-cert=cert.pgp --detached foo-sig.pgp foo.md
Good signature from 84B292ABCE27285B
1 good signature.
$ ls -l checked*
-rw-rw-r-- 1 liw liw 1086 Oct 15 16:23 checked.md
```

\newpage

```{.sh .numberLines}
$ sq encrypt --recipient-cert=cert.pgp --signer-key=key.pgp --output=bar.pgp foo.md
$ ls -l bar.pgp
-rw-rw-r-- 1 liw liw 2076 Oct 15 16:26 bar.pgp
```

```{.sh .numberLines}
$ sq decrypt --recipient-key=key.pgp --signer-cert=cert.pgp --output=decrypted.md bar.pgp
Encrypted using AES with 256-bit key
Compressed using ZIP
Good signature from 84B292ABCE27285B
1 good signature.
$ cmp foo.md decrypted.md
$ ls -l decrypted.md
-rw-rw-r-- 1 liw liw 1086 Oct 15 16:27 decrypted.md
```

# Introduction

## What are Sequoia-PGP and `sq`?

The Sequoia-PGP project works to make use of cryptography for privacy
and authentication in communication more commonplace. The project
produces and maintains an implementation of the OpenPGP standard
that's easy and uncomplicated to use.

OpenPGP is used widely in the IT industry and by free and open source
projects to verify the authenticity of software packages, and for
encrypting and authenticating messages.

`sq` is the command line tool provided by Sequoia-PGP. It's easy and
uncomplicated to use.

## Why use OpenPGP?

Cryptography is used for several reasons:

* to keep private things private
* to make sure data hasn't been changed
* to make sure from whom communication comes from

The OpenPGP standard provides for all these use cases, as well as
providing protocols and data formats for managing the ecosystem of
OpenPGP users. The standard specifies, for example, what form
cryptographic messages take. This allows different parties in a
communication can use different programs. One correspondent might be
using a laptop computer, and while another uses a mobile phone. The
software running on those will be radically different, but as long as
they follow the same standard, secure communication is no problem.


## Who this guide is aimed for?

This guide is aimed at those who want to
communicate privately, and receive and send messages and data safely with other
people. The guide does not require any background in cryptography,
mathematics, or programming, but does require using a computer via the
command line.


## Scope of this guide

This guide covers the important concepts in using cryptography as
specified by the OpenPGP standard:

* keys, certificates, and managing them
* certifying names attached to keys
* signing files and verifying signatures
* encrypting and decrypting data

The guide shows how to use the `sq` command line tool from
Sequoia-PGP. It does not cover integrating Sequoia-PGP with mail
software, version control, file transfer software, or other
applications. (That will be covered by other documentation.)

## Structure of the guide

This guide has the following structure:

* The prelude chapter is a guide for getting started quickly. It's
  aimed at readers who are already familiar with the relevant
  cryptographic concepts, having perhaps used other software, and just
  want to see how to do a few basic things. That chapter provides no
  explanation, just shows the commands. You can safely skip it if you
  want to.
* The appendixes show how to perform tasks to achieve specific goals.
  These are expanded versions of the prelude
  examples, but with detailed, step-by-step explanations of
  everything. These "how to" guides are useful for getting things
  done, without having to wade through long discussions about
  underlying concepts.
* There is also an appendix with a glossary, which can be helpful for
  looking up unknown terminology, and another appendix with links to
  additional material relevant to Sequoia-PGP and cryptography.
* The rest of this guide is discussions of the concepts needed
  to understand how cryptography works, and how to use it well.

Notably, this guide is not meant to be a reference guide. It does not
try to cover every aspect of the `sq` tool in detail. The built-in
help, which you can get by running `sq help` or `sq encrypt --help`,
is always up to date and a good way to look up details.


# Installing `sq`

This chapter explains how to install `sq` in various ways. It is by
necessity always going to be incomplete, but the authors would
gratefully accept changes for additional target systems.

## On various platforms

### Debian

On a Debian system (version 11 or later):

```
apt install sq
```

Note that on Debian 11 (bullseye), the version of `sq` is rather old.
You may want to install a back-ported version, from the usual Debian
location for them.

### Fedora
FIXME.

### Arch

FIXME.

### FreeBSD

FIXME.

### OpenBSD

FIXME.

### NetBSD

FIXME.

### macOS

FIXME.

### Windows

FIXME.

## From source code on all platforms

To build and install `sq` from source, you need to have the [Rust][]
toolchain installed, in particular `cargo` and `rustc`. You also need
a number of non-Rust build dependencies installed; see the
[README.md][] for an up to date list.

To build and install the latest released version of `sq`, run the
following command:

```{.sh .numberLines}
$ cargo install sequoia-sq
```

To build `sq` from the current development version, get its source
code from [GitLab][]:

```{.sh .numberLines}
$ git clone https://gitlab.com/sequoia-pgp/sequoia.git
$ cargo install --path=sequoia/sq
```

[GitLab]: https://gitlab.com/sequoia-pgp/sequoia
[Rust]: https://www.rust-lang.org/
[README.md]: https://gitlab.com/sequoia-pgp/sequoia/-/blob/main/README.md


# On public key cryptography {#public-key-cryptography}

The science of keeping private communications private, verifying the
source of messages, and verifying that messages haven't been modified
in transit, is called _cryptography_. It is not just for spies.
Cryptography allows everyday activities such as shopping and banking
to happen without rampant theft. It also allows journalists working on
stories about the rich, powerful, or corrupt to communicate with their
sources without fear of prematurely revealing what they're doing.

Cryptography use mathematics to turn messages into on opaque form and
back, using a secret key. The opaque form, also known as _ciphertext_,
is impossible for anyone to understand without the use of the key. As
long as the secret remains secret, the contents of communication is
kept secure.

A key is a very large random number used for encryption and digital
signatures. In public key cryptography a key consists of a private and
a public part. In `sq`, the private part is called just  _key_, and
you keep it secret. The public part is called a _certificate_ and
you're meant to share it with anyone who might use it.

The private and public parts (key and certificate) are tied together
with mathematics in such a way that they have a very useful feature:
if you encrypt something using the public part, the result can only be
decrypted with the private part. And also vice versa: if you encrypt
something using the private part, only the public part can decrypt
this. Since anyone can decrypt, this operation is called a _digital
signature_.

This allows some very interesting things:

* I can publish my certificate, and anyone can send me a confidential
  message by encrypting it using my certificate, and be assured that
  only I can read the message.

  Examples: Activists in oppressive regimes using this can stop their
  governments from eavesdropping on them. Journalists can protect
  communications with their sources. Corporations don't need to worry
  their trade secrets leak to competitors.

* I can send them a reply, encrypting it with my private key, and they
  can be sure it's from me, because they can decrypt it using my
  public certificate.

  Examples: The CFO can't be fooled by forged emails from the CEO
  telling them to pay a fraudulent invoice. Activists can't be fooled
  by messages from the secret police to go the town square at noon
  wearing a pink rose. Journalists can be sure the message is from
  their source and not someone who's trying to mess with reporting.

* These can be combined: If I have their certificate, I can be sure
  that the message they send me is really from them, and that only
  they can read my response.

* I can publish a software release, and encrypt the file using my
  private key. Anyone downloading the release can be sure they get
  what I published by decrypting it with my certificate.

The mathematical and cryptographic details of how this works are
outside the scope of this guide, but see the [references](#references)
for links to explanations.

## Symmetric cryptography

The alternative to public key cryptography is _symmetric
cryptography_. (Public key cryptography is also known as _asymmetric
cryptography_.) In symmetric cryptography there is no public and
private parts of an encryption key. There is only one part, and the
same key is used both for encrypting and for decrypting data. This is
in many ways easier to understand, but is usually more awkward to use.

Symmetric cryptography is best suited when only one person needs to
know the key, or when everyone who needs the key can get it in person.
The key in symmetric cryptography is also known as a _shared secret_.
This has several problems:

* when it comes to people, keeping secrets is difficult, and shared
  secrets especially so
* all parties need to meet someone in the inner circle, unless they
  already have a secure communication channel
* it's hard to remove someone from the inner circle: you need a new
  secret and you need to share it with everyone, except the unwanted
  person

Public key cryptography makes key management easier: everyone only has
their own secret to take care of, and new messages can be encrypted to
everyone except an unwanted person. Also, secure communication is
possible between parties previously unknown to each other.


aspect            symmetric     public key
-------           ----------    -----------
speed             fast          slow
key size          short         long
key management    hard          easy

Table: comparison of types of cryptography

The benefit is that symmetric cryptography tends to run faster and
have shorter keys compared to public key cryptography, for similar
levels of resistance against attacks.


## Limitations of cryptography

Whenever thinking about cryptography it's important to remember that
it has limitations. For example, no cryptography can prevent the
intended recipient from wilfully sharing an encrypted a message they
receive. If you send a photo of your safe combination to someone,
encrypted with their certificate, they can decrypt it, and share the
picture with the highest bidder.

Also, no cryptography provides any protection if keys aren't kept
private. If I accidentally publish my encryption key as a front page
advert on the New York Times, cryptography can't prevent others from
using that to decrypt messages intended for me, or publishing messages
that claim to be from me.

Further, cryptography doesn't protect against violence used to coerce
either party in a secure communication from disclosing secrets.

Finally, cryptography relies on some assumptions of what kind of
attacks are feasible, whether they're based on mathematics or raw
computing power. Over time, attacks on cryptographic protocols,
algorithms, and implementations only get stronger. Every few years,
attackers have a breakthrough, and some classes of cryptography
suddenly become so weak normal people can break them. Then the
purveyors and users of cryptography move to newer, stronger
alternatives.

For most people, these are quite unlikely scenarios. Most people do
not actually have enemies who are a threat specifically to them. If
you do, or you suspect you do, be very careful what you do and what
advice you follow. This guide is not likely to be all you need to
know.


# General principles of the `sq` interface

`sq` is a command line tool using subcommands and options. Global
options come before subcommand on the command line, and options
specific to the subcommand come after.

Some options have both long and short forms. Thus, for example,
`--output` may be shortened to just `-o` if the user wants to. The
examples in this guide use the long form, as that's clearer and easier
to understand without explanation. In practice, the short form is
often more convenient to write.

Some options are "flags", and the use of the option is enough. For
example, to request binary output, the option` --binary` is enough.
Other options require more information to be provided. For example,
the option `--recipient-key` to specify what key to use for a
recipient when encrypting needs to be provided the name of the file in
which the key is stored. Such option values can be specified as the
command line argument after the option, or appended to the option
itself using `=foo` syntax. Thus, the following are exactly
equivalent:

```
sq encrypt --recipient-cert cert.pgp --output bar.pgp foo.md
sq encrypt --recipient-cert=cert.pgp --output=bar.pgp foo.md
```

This guide uses the latter syntax to make it clearer when an option is
given a value without the reader having to look up each option.

The `sq` command has built-in help text that can be accessed using the
`help` command or the `--help` option:

```
sq help
sq --help
sq help key
sq key --help
sq help key generate
sq key generate --help
```

Use the help feature liberally to find out all the subcommands and
options, and whether an option is a flag or takes a value, and what
other arguments the command takes.



# Managing one's own key

This chapter concentrates on creating and managing a key for oneself.
Please see the [glossary](#glossary) for definitions of terms. Some of
the terminology `sq` uses is specific to cryptography in general, or
to OpenPGP, and some is specific to `sq` itself.

## Why use keys and certificates?

Your key is, in the context of public key cryptography, you. It's a
digital artifact that represents you to everyone else. Nobody else has
your key. You and your key are inseparable, and to everyone else, your
key is you and you are your key.

That is, of course, romantic balderdash. A key is a large random
number. It has no free will, it has no agency, it can't think, it
doesn't feel, it can't act, it can't enjoy a cup of hot tea in the
morning while writing a book, it's not alive. In no real way is it
you. Except when it comes to secure communication, your key stands for
you. When someone wants to send a confidential message to you, they
encrypt it using your certificate, which is mathematically,
inalienably linked to your key. When you want to prove a message comes
from you, you encrypt it with your key.

You can have as many keys as you want. You can have one for work,
another for school, a third for you family and friends, and fourth one
for publishing poetry online. These keys may be linked or kept
separate, as you prefer.

## Types of keys and algorithms

Over time, as cryptographic attacks have weakened the protections of
cryptographic defences, different types of keys and algorithms have
been developed and enhanced. While a through discussion of these is
beyond the scope of this guide, the list below gives a summary.

* Encryption algorithm **RSA**: this is the oldest known public key
  encryption algorithm. It has stayed strong, except as attackers are
  gaining faster computers and better attack software, the keys used
  for RSA have needed to become much longer. Where a 384 bit key was
  OK in the early 1990s, a 2048 bit or even a 4096 bit key is
  preferred thirty years later. The longer an RSA key is, the more
  computing power it takes to use it, and this makes strong
  cryptography with RSA slower.

* **Elliptic curve cryptography**: where RSA relies on the
  computational difficulty of factoring large numbers, elliptic curve
  relies on certain operations in geometry involving elliptical curves
  in two-dimensional space being hard to undo. There are several
  curves in popular use, and some of the older ones are now considered
  to provide only weak protection against attackers. New curves get
  developed from time to time, to provide more strength. With elliptic
  curves, the choice of curve also specifies the size of the key.

* **Checksum** algorithms are one-way mathematical functions, where if
  you have a file you can compute a result from its contents easily,
  but if you have the result, it's really hard to get back the file. A
  _cryptographic checksum_ is one where it's really hard to find any
  file with the given checksum result. As with encryption algorithms,
  checksums need to be made stronger as time goes by. OpenPGP
  originally mandated use of the MD5 algorithm, but that is no longer
  suitable for use. Currently SHA256 is the preferred one.

Because key types and algorithms need to be improved over time, the
OpenPGP standard allows replacing them in newer versions of the
standard, without fundamentally changing the structure of the OpenPGP
protocol. Each version of OpenPGP supports multiple key types and
algorithms, allowing for a managed migration towards stronger
security, and without losing access to older files and messages.


## Why use subkeys?

Even someone having only one cryptographic key may benefit from having
other keys for specific purposes. For example, they might have a very
strong primary key as their primary key, and additional, auxiliary
keys for encryption or digital signatures. Such auxiliary keys can be
tied to the primary key using _certifications_, which we'll cover in
more detail later. For now, a certification uses the primary key to
declare that the auxiliary key can be used instead of the primary key
for a specific purpose. The auxiliary key then becomes a _subkey_,
and other users of OpenPGP will use it automatically, if they have
your certificate. This setup has several benefits:

* you can have separate subkeys for encryption, signing, or
  authentication
* you can use a smaller key when less security is OK in exchange for
  faster use
* you can have a separate subkey for each device you have, or put
  them on a hardware security token
* you can replace the subkeys easily: others trust your certificate,
  and will happily use any subkey they can verify using your
  certificate

All of this is managed pretty much automatically using OpenPGP
software.


## Why would keys expire automatically?

A key, whether primary or subkey, can be set to expire at a given
time. This is a precaution against you losing access to the primary
key: if the key expires, others won't use it anymore. You can extend
the expiration as often as you wish, although that requires getting
your update certificate to everyone who needs to use it.

You can also set subkeys to expire. This has the same benefits as
expiring the primary key.

Changing expiration times can be a chore. There's a security benefit
to it, but if it's inconvenient for you, you may want to consider not
expiring keys, or only expire subkeys.


## Generating a key

To generate a key with `sq`:

```sh
sq key generate --userid="My Name <me@example.com>" --export=key.pgp
```

The _user id_ should usually have the form "`Name <email@address>`".
The software doesn't mandate this, but much software that uses OpenPGP
relies on the convention. For example, when a email program looks up a
certificate for a recipient, it uses the email address to do so. If
the user id does not include the email address, the lookup fails.

A key can have several user ids, which is handy for people who have
several email addresses, at once or over time.

You can set an expiration time at the time of creating a key, if you
want. See the `--expires` and `--expires-in` options.

Generating a key with `sq` results in two files. The key is put in the
file you name as the argument to the `--export` option (or you can
specify the name yourself, with the `--rev-cert` option). A _key
revocation certificate_ is written to a file with that name and `.rev`
appended to it. The revocation certificate tells others that your key
is longer usable. If, for example, you lose the file with the key, you
can share the revocation certificate with others, and they (or their
OpenPGP software) will know to not use that key anymore. We'll cover
key revocation in more detail later.

You can choose the cryptographic algorithm, and whether the key should
have subkeys for signing or encrypting messages. See the `--help`
output for the option names.


## Extracting a certificate from a key

Given a key, you can extract the certificate linked to it:

```
sq key extract-cert --output=cert.pgp key.pgp
```

The `cert.pgp` file is the certificate (choose whatever name you want
for it). You need to re-extract the certificate every time you make a
change to the key that would shared with others: user ids, expiration
times, subkeys.

Note that while one can extract a certificate from a key, the other
direction is not possible.


## Sharing one's certificate with others

A certificate contains no secrets and you can safely share it with
anyone: include it as an attachment in every email you send; put it on
your web home page; put it on your profile on social media sites such
as Facebook, Twitter, Mastodon, or GitHub; publish a photo of it on
photo sharing site; print it on business cards. We'll cover more
options later in the chapter on managing keys in a community.

A caveat: a certificate does contain all the user ids on your key, so
if any of those is not public information you may want to remove them
from your key before extracting the certificate. You may want to have
an entirely separate key for that. User ids are tied to the primary
key, subkeys inherit them from their primary.

# Using digital signatures

## Why use signatures?

Digital signatures are used to show who sent a message and that it
hasn't been changed. See the chapter on [public key
cryptography](#public-key-cryptography) for a longer discussion.

It's important to note that signatures are good not just for messages,
but for any kind of data, including files and cryptographic keys.

* You can sign a file to prove that you've seen it. Others can then
  verify that the signature and file match, and trust that the file
  they have is the same one you signed.

  This does not prove that you created the file, only that you had the
  file at the time you signed it.

* When a software distributor, such as Debian, Fedora, Red Hat, or the
  Apple app store, prepares a software package for distribution, they
  sign it using the their key. When a system downloads a
  package from the distributor, it verifies the package signature
  using the distributor's public key, which it already has, as it
  comes pre-installed in Linux distributions and on Apple devices. If
  the package fails to match its signature, the package won't be
  installed.

  This is done to protect the system installing software from
  maliciously modified software. It can also be used as a mechanism
  for preventing software to be installed that isn't approved by the
  distributor. Linux distributions don't do that, but big corporations
  seeking a dominant market position tend to like it.

* When you create a new key, any user ids attached to it are signed by
  the key. This prevents other people from adding their email address
  to your key. If they could do that, they would get your email. While
  they can't read it, if it's encrypted, they can at least make sure
  you don't get it.

* When you add subkeys, they are signed by the primary key to prove
  that you, the key holder, wants the subkey to be used.

## Making a signature

To sign a file with `sq`, you need your key (not the certificate). The
command to sign is:

```sh
sq sign --signer-key=key.pgp --output=foo.pgp foo.md
```

This signs the file `foo.md`, and writes the signed file to `foo.pgp`.
That file contains both the contents of `foo.md` and a signature.

Having the signature be part of the file can be convenient, but it can
also be inconvenient. Sometimes it's easier to have the signature
separate from the data. That's called a _detached signature_. If
nothing else it means that you don't have two copies of the data,
which can be costly in terms of disk space. A detached signature is
also handy if someone else already has a copy of the data and you just
want to prove your copy is identical.

To make a detached signature:

```
sq sign --detached --signer-key=key.pgp --output=foo-sig.pgp foo.md
```

Note the `--detached` option. The signature, but none of the original
data, is written to `foo-sig.pgp`. The detached signature is small,
and its size does not depend on the size of the signed data. That's
because the detached signature contains the _cryptographic checksum_
of the original data.

## Verifying a signature

Verifying the signature of a signed file is done like this:

```
sq verify --signer-cert=cert.pgp --output=checked.md foo.pgp
```

The output will say something like this:

```
Good signature from 84B292ABCE27285B
1 good signature.
```

The mysterious number `84B292ABCE27285B` is the key identifier of the
key who made the signature. It should match the certificate you've
provided.

If the signature doesn't verify correctly, the error message is clear
(see [#768](https://gitlab.com/sequoia-pgp/sequoia/-/issues/768)):

```
thread 'main' panicked at 'It is an error to consume more than data returns: Custom { kind: InvalidInput, error: "Bad CRC sum." }', openpgp/src/parse.rs:4988:18
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```

(That output is a bug. See issue
[#768](https://gitlab.com/sequoia-pgp/sequoia/-/issues/768). This
part of the guide will be updated once the bug is fixed.)

A detached signature is verified in a similar fashion, but you need to
be a little more verbose and give the names of both the data file and
the signature file:

```
$ sq verify --detached --signer-cert=cert.pgp foo-sig.pgp foo.md
```

## Trusting a certificate

If you have a suspicious mind set, you may have spotted a glaring
error in the above discussion of verifying digital signatures: you
must have the certificate of the sender, and you need to be sure it's
actually their certificate.

Sometimes that's easy. If you're downloading software to install using
a "package manager" (Linux distributions), or the app installation
program on a mobile phone, the certificate was pre-installed.

If you're downloading software directly from its publisher's website,
they probably provide the certificate on their website as well. If
you're downloading things over HTTPS (instead of the unencrypted,
unprotected, un-lamented plain HTTP), you can _probably_ trust the
certificate. It's possible to spoof that, if not easy.

You may gain more trust in the certificate by verifying that the one
you have is one that a lot of other people have, and have had a long
time. You can do this by asking a lot of people. We'll return to this
topic later in the chapter on managing keys at a community level.


# Using encryption

## Encrypting a file

Encrypting things is how you keep your private things private. There's
a large, global debate about privacy and who deserves to keep what
private and how to still catch bad busy who harm others. We shan't get
into that here.

To encrypt a file using `sq`:

```
sq encrypt --recipient-cert=cert.pgp --output=bar.pgp foo.md
```

This encrypts the file `foo.md`, using the certificate in `cert.pgp`,
and writes the result into `bar.pgp`.

Note that the encryption is done only for the explicitly specified
recipients. If you want to read the encrypted output yourself later,
you need to add yourself as a recipient.

The output file has a name different from the input file so that the
filename, which is not encrypted, does not reveal anything about the
contents to someone who happens to see it.

You can optionally also sign the data by adding the
`--signer-key=key.pgp` option to the encryption command.

## Decrypting a file

To decrypt an encrypted file:

```
sq decrypt --recipient-key=key.pgp --output=decrypted.md bar.pgp
```

The output is written to `decrypted.md`. If the encrypted data was
also signed, and you add the `--signer-cert=cert.pgp` option, the
decryption will check the signature. If the signature fails to match,
the data is not written into the output file to avoid anyone trusting
an unauthenticated message.



# Managing digital keys and certificates on a community level

FIXME: This chapter will discuss how to manage keys and certificates
among large groups of people. It will discuss how to build strong
trust that a key belongs to a specific person or organization. It will
discuss various ways of distributing certificates.


* how do I find someone's key?
* how do I check it's their key?
* how do I get updates to their key?
  - user ids
  - subkeys
  - expiration changes
  - revocation of primary key or subkey



# Appendix: How to...? {.unnumbered}

This appendix has task oriented guides for achieving specific goals.
Each how-to guide will explain every step, giving command line
examples, but will not go into detail about what is happening or why.
These how-to guides are aimed at people who need to achieve a specific
goal, have some understanding of OpenPGP concepts, but don't currently
care to understand deeply. As such, the how-to guides will repeat
specifics that have been covered in the rest of the book.

## How to verify that a downloaded file is the one its author made
## How to sign a file to share with others
## How to decrypt a message from someone else
## How to encrypt a message for someone else
## How to generate a key, with subkeys, and a certificate
## How to distribute certificate to others
## How to certify someone else's user id



# Appendix: Switching from GnuPG to Sequoia-PGP {.unnumbered}

This appendix is aimed at people who already know how to use `gpg`,
the command line tool from GnuPG that roughly corresponds to `sq`. It
shows how to do specific tasks using either `gpg` or `sq`. It will

GnuPG stores keys and certificates in the `~/.gnupg` directory, or
directory named in the `GNUPGHOME` environment variable. They're not
easily accessed directly as files, and are referred to via the user id
or using a hexadecimal key identifier or key finger print. The set of
keys and certificates in that directory is called a _keyring_. possibly
be a comparison table, for easy review.

GnuPG typically outputs binary files. The `--armor` option tells it to
write a textual representation. That representation is still not
human-readable, but can be easier to transmit over various channels
that expect text instead of binary data.

## Generate a key and certificate

```
gpg --quick-gen-key "Tomjon <tomjon@example.com>"
```

## Export certificate into a file

```
gpg --export --armor tomjon > tomjon.asc
```

## Import a certificate into your keyring

```
gpg --import certificate.asc
```

## List all certificates in your keyring

Either all keys, or keys with a user id that contains a string:

```
gpg --list-keys
gpg --list-keys tomjon
```

Output for one key looks something like:

```
pub   rsa4096 2015-03-01 [SC] [expires: 2025-01-10]
      DBE5439D97D8262664A1B01844E17740B8611E9C
uid           [ unknown] Lars Wirzenius <liw@liw.fi>
uid           [ unknown] Lars Wirzenius <liw@iki.fi>
uid           [ unknown] Lars Wirzenius <lwirzenius@wikimedia.org>
sub   rsa4096 2015-03-01 [S]
sub   rsa4096 2015-03-01 [E]
```

The first word of the line tells you what the line contains:

* `pub`---a public key (i.e., certificate)
* `sec`---a secret key (i.e., key, in `--list-secret-keys` output)
* `sec#`---a secret key is known to exist, but isn't actually in the keyring
* `uid`---a userid attached to the key
* `sub`---a subkey

The `unknown` tells you how much you've told GnuPG you trust that user
id. There's also information about type and length of a key, what it's
used for (signing, certifying, encrypting), and key fingerprint
(`DBE5439D97D8262664A1B01844E17740B8611E9C` above). The fingerprint is
the strongest way to refer to a key.

## List all private keys in your keyring

```
gpg --list-secret-keys
```

## Sign a file

Drop `--armor` for binary output. Output goes to `hello.txt.asc` with
`--armor`, or `hello.txt.pgp` without.

```
gpg --sign --armor hello.txt
```

## Check a file's signature

```
gpg --verify hello.txt.gpg
```


## Sign a file---detached signature

Drop `--armor` for binary output. Output goes to `hello.txt.asc`
with `--armor`, or `hello.txt.sig` without.

```
gpg --detach-sign --armor hello.txt
```

## Check a file's detached signature

```
gpg --verify hello.txt.sig hello.txt
```

## Encrypt a file

Output goes to `hello.txt.pgp` (with `--armor` to `hello.txt.asc`).
The `--recipient` option can be shortened to `-r`. By default, this
encrypts _only_ for the explicitly named recipients, so if one wants
to decrypt the file later oneself, one needs to remember to encrypt it
for oneself.

```
gpg --encrypt --recipient liw -r tomjon hello.txt
gpg --encrypt --armor --recipient liw -r tomjon hello.txt
```

## Decrypt a file

Output goes to the standard output unless `--output` is used. Note
that GnuPG may output the cleartext, even if the signature fails.

```
gpg --decrypt hello.txt.gpg
```

# Appendix: Glossary {#glossary .unnumbered}

This appendix explains all the specialist terminology related to
OpenPGP and Sequoia-PGP. It includes both the terms Sequoia prefers
(e.g., "certificate") and the older terminology for the same thing
("public key").

authenticate

: to verify the origin of a message, using a digital signature

certificate

: the public key in public key cryptography; meant to be distributed
  widely; _see_ public key

certification

: assuring that the user id and key belong to a specific person, using
  a signature on the user id

cipher

: an encryption algorithm

cleartext

: data that has not been encrypted, even if it's not text

decrypt

: convert encrypted data into cleartext

encrypt

: convert cleartext data into a form nobody can read unless it was
  encrypted for them

identity

: a deep philosophial problem; for OpenPGP, the user id attached to
  one's key

key

: a key for encrypting and decrypting data; _see_ private key, public key

key pair

: in public key cryptography, the two parts of a key; _see_ private
  key, public key

private key

: the private part of a key in public key cryptography; this is meant
  to not be shared with anyone

public key

: the public part of a key in public key cryptography; _see_ certificate

sign

: to encrypt data with one's private key, to be decrypted using one's
  certificate

signature

: the result of signing data; _see_ sign

user id

: a name and/or email address attatched to a key; there can be many
  such user ids attached to one key

verify

: to check that signed data matches its signature



# Appendix: References {#references .unnumbered}

* [OpenPGP on Wikipedia](https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP)
* [PGP on Wikipedia](https://en.wikipedia.org/wiki/Pretty_Good_Privacy)
* [Public key cryptography on Wikipedia](https://en.wikipedia.org/wiki/Public-key_cryptography)
* [RFC 4880](https://datatracker.ietf.org/doc/html/rfc4880), the
  Internet standard specification for OpenPGP
* [Sequoia-PGP website](https://sequoia-pgp.org/)


# Appendix: Copyright license {.unnumbered}

Copyright 2021  The pep foundation

This guide is licensed under the Creative Commons
Attirubiont-ShareAlike (CC-BY-SA) 4.0 International license. The
license is reproduced below. It is copied from
<https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt>.

```
Attribution-ShareAlike 4.0 International

=======================================================================

Creative Commons Corporation ("Creative Commons") is not a law firm and
does not provide legal services or legal advice. Distribution of
Creative Commons public licenses does not create a lawyer-client or
other relationship. Creative Commons makes its licenses and related
information available on an "as-is" basis. Creative Commons gives no
warranties regarding its licenses, any material licensed under their
terms and conditions, or any related information. Creative Commons
disclaims all liability for damages resulting from their use to the
fullest extent possible.

Using Creative Commons Public Licenses

Creative Commons public licenses provide a standard set of terms and
conditions that creators and other rights holders may use to share
original works of authorship and other material subject to copyright
and certain other rights specified in the public license below. The
following considerations are for informational purposes only, are not
exhaustive, and do not form part of our licenses.

     Considerations for licensors: Our public licenses are
     intended for use by those authorized to give the public
     permission to use material in ways otherwise restricted by
     copyright and certain other rights. Our licenses are
     irrevocable. Licensors should read and understand the terms
     and conditions of the license they choose before applying it.
     Licensors should also secure all rights necessary before
     applying our licenses so that the public can reuse the
     material as expected. Licensors should clearly mark any
     material not subject to the license. This includes other CC-
     licensed material, or material used under an exception or
     limitation to copyright. More considerations for licensors:
    wiki.creativecommons.org/Considerations_for_licensors

     Considerations for the public: By using one of our public
     licenses, a licensor grants the public permission to use the
     licensed material under specified terms and conditions. If
     the licensor's permission is not necessary for any reason--for
     example, because of any applicable exception or limitation to
     copyright--then that use is not regulated by the license. Our
     licenses grant only permissions under copyright and certain
     other rights that a licensor has authority to grant. Use of
     the licensed material may still be restricted for other
     reasons, including because others have copyright or other
     rights in the material. A licensor may make special requests,
     such as asking that all changes be marked or described.
     Although not required by our licenses, you are encouraged to
     respect those requests where reasonable. More considerations
     for the public:
    wiki.creativecommons.org/Considerations_for_licensees

=======================================================================

Creative Commons Attribution-ShareAlike 4.0 International Public
License

By exercising the Licensed Rights (defined below), You accept and agree
to be bound by the terms and conditions of this Creative Commons
Attribution-ShareAlike 4.0 International Public License ("Public
License"). To the extent this Public License may be interpreted as a
contract, You are granted the Licensed Rights in consideration of Your
acceptance of these terms and conditions, and the Licensor grants You
such rights in consideration of benefits the Licensor receives from
making the Licensed Material available under these terms and
conditions.


Section 1 -- Definitions.

  a. Adapted Material means material subject to Copyright and Similar
     Rights that is derived from or based upon the Licensed Material
     and in which the Licensed Material is translated, altered,
     arranged, transformed, or otherwise modified in a manner requiring
     permission under the Copyright and Similar Rights held by the
     Licensor. For purposes of this Public License, where the Licensed
     Material is a musical work, performance, or sound recording,
     Adapted Material is always produced where the Licensed Material is
     synched in timed relation with a moving image.

  b. Adapter's License means the license You apply to Your Copyright
     and Similar Rights in Your contributions to Adapted Material in
     accordance with the terms and conditions of this Public License.

  c. BY-SA Compatible License means a license listed at
     creativecommons.org/compatiblelicenses, approved by Creative
     Commons as essentially the equivalent of this Public License.

  d. Copyright and Similar Rights means copyright and/or similar rights
     closely related to copyright including, without limitation,
     performance, broadcast, sound recording, and Sui Generis Database
     Rights, without regard to how the rights are labeled or
     categorized. For purposes of this Public License, the rights
     specified in Section 2(b)(1)-(2) are not Copyright and Similar
     Rights.

  e. Effective Technological Measures means those measures that, in the
     absence of proper authority, may not be circumvented under laws
     fulfilling obligations under Article 11 of the WIPO Copyright
     Treaty adopted on December 20, 1996, and/or similar international
     agreements.

  f. Exceptions and Limitations means fair use, fair dealing, and/or
     any other exception or limitation to Copyright and Similar Rights
     that applies to Your use of the Licensed Material.

  g. License Elements means the license attributes listed in the name
     of a Creative Commons Public License. The License Elements of this
     Public License are Attribution and ShareAlike.

  h. Licensed Material means the artistic or literary work, database,
     or other material to which the Licensor applied this Public
     License.

  i. Licensed Rights means the rights granted to You subject to the
     terms and conditions of this Public License, which are limited to
     all Copyright and Similar Rights that apply to Your use of the
     Licensed Material and that the Licensor has authority to license.

  j. Licensor means the individual(s) or entity(ies) granting rights
     under this Public License.

  k. Share means to provide material to the public by any means or
     process that requires permission under the Licensed Rights, such
     as reproduction, public display, public performance, distribution,
     dissemination, communication, or importation, and to make material
     available to the public including in ways that members of the
     public may access the material from a place and at a time
     individually chosen by them.

  l. Sui Generis Database Rights means rights other than copyright
     resulting from Directive 96/9/EC of the European Parliament and of
     the Council of 11 March 1996 on the legal protection of databases,
     as amended and/or succeeded, as well as other essentially
     equivalent rights anywhere in the world.

  m. You means the individual or entity exercising the Licensed Rights
     under this Public License. Your has a corresponding meaning.


Section 2 -- Scope.

  a. License grant.

       1. Subject to the terms and conditions of this Public License,
          the Licensor hereby grants You a worldwide, royalty-free,
          non-sublicensable, non-exclusive, irrevocable license to
          exercise the Licensed Rights in the Licensed Material to:

            a. reproduce and Share the Licensed Material, in whole or
               in part; and

            b. produce, reproduce, and Share Adapted Material.

       2. Exceptions and Limitations. For the avoidance of doubt, where
          Exceptions and Limitations apply to Your use, this Public
          License does not apply, and You do not need to comply with
          its terms and conditions.

       3. Term. The term of this Public License is specified in Section
          6(a).

       4. Media and formats; technical modifications allowed. The
          Licensor authorizes You to exercise the Licensed Rights in
          all media and formats whether now known or hereafter created,
          and to make technical modifications necessary to do so. The
          Licensor waives and/or agrees not to assert any right or
          authority to forbid You from making technical modifications
          necessary to exercise the Licensed Rights, including
          technical modifications necessary to circumvent Effective
          Technological Measures. For purposes of this Public License,
          simply making modifications authorized by this Section 2(a)
          (4) never produces Adapted Material.

       5. Downstream recipients.

            a. Offer from the Licensor -- Licensed Material. Every
               recipient of the Licensed Material automatically
               receives an offer from the Licensor to exercise the
               Licensed Rights under the terms and conditions of this
               Public License.

            b. Additional offer from the Licensor -- Adapted Material.
               Every recipient of Adapted Material from You
               automatically receives an offer from the Licensor to
               exercise the Licensed Rights in the Adapted Material
               under the conditions of the Adapter's License You apply.

            c. No downstream restrictions. You may not offer or impose
               any additional or different terms or conditions on, or
               apply any Effective Technological Measures to, the
               Licensed Material if doing so restricts exercise of the
               Licensed Rights by any recipient of the Licensed
               Material.

       6. No endorsement. Nothing in this Public License constitutes or
          may be construed as permission to assert or imply that You
          are, or that Your use of the Licensed Material is, connected
          with, or sponsored, endorsed, or granted official status by,
          the Licensor or others designated to receive attribution as
          provided in Section 3(a)(1)(A)(i).

  b. Other rights.

       1. Moral rights, such as the right of integrity, are not
          licensed under this Public License, nor are publicity,
          privacy, and/or other similar personality rights; however, to
          the extent possible, the Licensor waives and/or agrees not to
          assert any such rights held by the Licensor to the limited
          extent necessary to allow You to exercise the Licensed
          Rights, but not otherwise.

       2. Patent and trademark rights are not licensed under this
          Public License.

       3. To the extent possible, the Licensor waives any right to
          collect royalties from You for the exercise of the Licensed
          Rights, whether directly or through a collecting society
          under any voluntary or waivable statutory or compulsory
          licensing scheme. In all other cases the Licensor expressly
          reserves any right to collect such royalties.


Section 3 -- License Conditions.

Your exercise of the Licensed Rights is expressly made subject to the
following conditions.

  a. Attribution.

       1. If You Share the Licensed Material (including in modified
          form), You must:

            a. retain the following if it is supplied by the Licensor
               with the Licensed Material:

                 i. identification of the creator(s) of the Licensed
                    Material and any others designated to receive
                    attribution, in any reasonable manner requested by
                    the Licensor (including by pseudonym if
                    designated);

                ii. a copyright notice;

               iii. a notice that refers to this Public License;

                iv. a notice that refers to the disclaimer of
                    warranties;

                 v. a URI or hyperlink to the Licensed Material to the
                    extent reasonably practicable;

            b. indicate if You modified the Licensed Material and
               retain an indication of any previous modifications; and

            c. indicate the Licensed Material is licensed under this
               Public License, and include the text of, or the URI or
               hyperlink to, this Public License.

       2. You may satisfy the conditions in Section 3(a)(1) in any
          reasonable manner based on the medium, means, and context in
          which You Share the Licensed Material. For example, it may be
          reasonable to satisfy the conditions by providing a URI or
          hyperlink to a resource that includes the required
          information.

       3. If requested by the Licensor, You must remove any of the
          information required by Section 3(a)(1)(A) to the extent
          reasonably practicable.

  b. ShareAlike.

     In addition to the conditions in Section 3(a), if You Share
     Adapted Material You produce, the following conditions also apply.

       1. The Adapter's License You apply must be a Creative Commons
          license with the same License Elements, this version or
          later, or a BY-SA Compatible License.

       2. You must include the text of, or the URI or hyperlink to, the
          Adapter's License You apply. You may satisfy this condition
          in any reasonable manner based on the medium, means, and
          context in which You Share Adapted Material.

       3. You may not offer or impose any additional or different terms
          or conditions on, or apply any Effective Technological
          Measures to, Adapted Material that restrict exercise of the
          rights granted under the Adapter's License You apply.


Section 4 -- Sui Generis Database Rights.

Where the Licensed Rights include Sui Generis Database Rights that
apply to Your use of the Licensed Material:

  a. for the avoidance of doubt, Section 2(a)(1) grants You the right
     to extract, reuse, reproduce, and Share all or a substantial
     portion of the contents of the database;

  b. if You include all or a substantial portion of the database
     contents in a database in which You have Sui Generis Database
     Rights, then the database in which You have Sui Generis Database
     Rights (but not its individual contents) is Adapted Material,

     including for purposes of Section 3(b); and
  c. You must comply with the conditions in Section 3(a) if You Share
     all or a substantial portion of the contents of the database.

For the avoidance of doubt, this Section 4 supplements and does not
replace Your obligations under this Public License where the Licensed
Rights include other Copyright and Similar Rights.


Section 5 -- Disclaimer of Warranties and Limitation of Liability.

  a. UNLESS OTHERWISE SEPARATELY UNDERTAKEN BY THE LICENSOR, TO THE
     EXTENT POSSIBLE, THE LICENSOR OFFERS THE LICENSED MATERIAL AS-IS
     AND AS-AVAILABLE, AND MAKES NO REPRESENTATIONS OR WARRANTIES OF
     ANY KIND CONCERNING THE LICENSED MATERIAL, WHETHER EXPRESS,
     IMPLIED, STATUTORY, OR OTHER. THIS INCLUDES, WITHOUT LIMITATION,
     WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR
     PURPOSE, NON-INFRINGEMENT, ABSENCE OF LATENT OR OTHER DEFECTS,
     ACCURACY, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT
     KNOWN OR DISCOVERABLE. WHERE DISCLAIMERS OF WARRANTIES ARE NOT
     ALLOWED IN FULL OR IN PART, THIS DISCLAIMER MAY NOT APPLY TO YOU.

  b. TO THE EXTENT POSSIBLE, IN NO EVENT WILL THE LICENSOR BE LIABLE
     TO YOU ON ANY LEGAL THEORY (INCLUDING, WITHOUT LIMITATION,
     NEGLIGENCE) OR OTHERWISE FOR ANY DIRECT, SPECIAL, INDIRECT,
     INCIDENTAL, CONSEQUENTIAL, PUNITIVE, EXEMPLARY, OR OTHER LOSSES,
     COSTS, EXPENSES, OR DAMAGES ARISING OUT OF THIS PUBLIC LICENSE OR
     USE OF THE LICENSED MATERIAL, EVEN IF THE LICENSOR HAS BEEN
     ADVISED OF THE POSSIBILITY OF SUCH LOSSES, COSTS, EXPENSES, OR
     DAMAGES. WHERE A LIMITATION OF LIABILITY IS NOT ALLOWED IN FULL OR
     IN PART, THIS LIMITATION MAY NOT APPLY TO YOU.

  c. The disclaimer of warranties and limitation of liability provided
     above shall be interpreted in a manner that, to the extent
     possible, most closely approximates an absolute disclaimer and
     waiver of all liability.


Section 6 -- Term and Termination.

  a. This Public License applies for the term of the Copyright and
     Similar Rights licensed here. However, if You fail to comply with
     this Public License, then Your rights under this Public License
     terminate automatically.

  b. Where Your right to use the Licensed Material has terminated under
     Section 6(a), it reinstates:

       1. automatically as of the date the violation is cured, provided
          it is cured within 30 days of Your discovery of the
          violation; or

       2. upon express reinstatement by the Licensor.

     For the avoidance of doubt, this Section 6(b) does not affect any
     right the Licensor may have to seek remedies for Your violations
     of this Public License.

  c. For the avoidance of doubt, the Licensor may also offer the
     Licensed Material under separate terms or conditions or stop
     distributing the Licensed Material at any time; however, doing so
     will not terminate this Public License.

  d. Sections 1, 5, 6, 7, and 8 survive termination of this Public
     License.


Section 7 -- Other Terms and Conditions.

  a. The Licensor shall not be bound by any additional or different
     terms or conditions communicated by You unless expressly agreed.

  b. Any arrangements, understandings, or agreements regarding the
     Licensed Material not stated herein are separate from and
     independent of the terms and conditions of this Public License.


Section 8 -- Interpretation.

  a. For the avoidance of doubt, this Public License does not, and
     shall not be interpreted to, reduce, limit, restrict, or impose
     conditions on any use of the Licensed Material that could lawfully
     be made without permission under this Public License.

  b. To the extent possible, if any provision of this Public License is
     deemed unenforceable, it shall be automatically reformed to the
     minimum extent necessary to make it enforceable. If the provision
     cannot be reformed, it shall be severed from this Public License
     without affecting the enforceability of the remaining terms and
     conditions.

  c. No term or condition of this Public License will be waived and no
     failure to comply consented to unless expressly agreed to by the
     Licensor.

  d. Nothing in this Public License constitutes or may be interpreted
     as a limitation upon, or waiver of, any privileges and immunities
     that apply to the Licensor or You, including from the legal
     processes of any jurisdiction or authority.


=======================================================================

Creative Commons is not a party to its public
licenses. Notwithstanding, Creative Commons may elect to apply one of
its public licenses to material it publishes and in those instances
will be considered the “Licensor.” The text of the Creative Commons
public licenses is dedicated to the public domain under the CC0 Public
Domain Dedication. Except for the limited purpose of indicating that
material is shared under a Creative Commons public license or as
otherwise permitted by the Creative Commons policies published at
creativecommons.org/policies, Creative Commons does not authorize the
use of the trademark "Creative Commons" or any other trademark or logo
of Creative Commons without its prior written consent including,
without limitation, in connection with any unauthorized modifications
to any of its public licenses or any other arrangements,
understandings, or agreements concerning use of licensed material. For
the avoidance of doubt, this paragraph does not form part of the
public licenses.

Creative Commons may be contacted at creativecommons.org.

```
